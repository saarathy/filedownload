package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CarsEntity")
public class CarEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "car_id")
    private int carid;
    @Column(name = "car_name")
    private String carname;
    @Column(name = "car_model")
    private int carmodel;
    @Column(name = "car_colour")
    private String carcolour;
    
    
	public int getCarid() {
		return carid;
	}
	public void setCarid(int carid) {
		this.carid = carid;
	}
	public int getCarmodel() {
		return carmodel;
	}
	public void setCarmodel(int carmodel) {
		this.carmodel = carmodel;
	}
	public String getCarname() {
		return carname;
	}
	public void setCarname(String carname) {
		this.carname = carname;
	}
	public String getCarcolour() {
		return carcolour;
	}
	public void setCarcolour(String carcolour) {
		this.carcolour = carcolour;
	}

	public CarEntity() {
		
	}

	public CarEntity(int carid, String carname, int carmodel, String carcolour) {
		super();
		this.carid = carid;
		this.carname = carname;
		this.carmodel = carmodel;
		this.carcolour = carcolour;
	}
	@Override
	public String toString() {
		return "CarEntity [carid=" + carid + ", carname=" + carname + ", carmodel=" + carmodel + ", carcolour="
				+ carcolour + "]";
	}
	
}
