package com.example.demo.Helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.entity.CarEntity;


public class CarHelper {
  public static String TYPE = "text/csv";
  static String[] HEADERs = { "car_id", "car_name", "car_model", "car_colour" };

  public static boolean hasCSVFormat(MultipartFile file) {

    if (!TYPE.equals(file.getContentType())) {
      return false;
    }

    return true;
  }

  public static List<CarEntity> csvToCar(InputStream is) {
    try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
        CSVParser csvParser = new CSVParser(fileReader,
            CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

      List<CarEntity> cars = new ArrayList<CarEntity>();

      Iterable<CSVRecord> csvRecords = csvParser.getRecords();

      for (CSVRecord csvRecord : csvRecords) {
    	  CarEntity car = new CarEntity(
              Integer.parseInt(csvRecord.get("car_id")),
              csvRecord.get("car_name"),
              Integer.parseInt(csvRecord.get("car_model")),
              csvRecord.get("car_colour")
            );

        cars.add(car);
      }

      return cars;
    } catch (IOException e) {
      throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
    }
  }}