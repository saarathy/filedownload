package com.example.demo.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.Helper.CarHelper;
import com.example.demo.Repository.CarRepo;
import com.example.demo.entity.CarEntity;

@Service
@Component
public class CarService {
  
	@Autowired
	CarRepo repo;
	
	  public void save(MultipartFile file) {
		    try {
		      List<CarEntity> cars = CarHelper.csvToCar(file.getInputStream());
		      repo.saveAll(cars);
		    } catch (IOException e) {
		      throw new RuntimeException("fail to store csv data: " + e.getMessage());
		    }
		  }

		  public List<CarEntity> getAllTutorials() {
		    return repo.findAll();
		  }

//		public Object postDetails(CarEntity carEntity) {
//			return repo.save(carEntity);
//		}	
}
